<?php

namespace Acg\TesteBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository/ProdutosRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProdutosRepository extends EntityRepository
{
    public function buscaPorTitulo($titulo)
    {
        $query = $this->getEntityManager()->createQuery('
                  SELECT p FROM AcgTesteBundle:Produtos p JOIN p.categorias c WHERE c.titulo LIKE :titulo
                  ');

        $query->setParameters(array(
            'titulo' => '%' . $titulo . '%'
        ));
        //var_dump($query->getSQL());die;
        return $query->getResult();
    }
}

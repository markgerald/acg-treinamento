<?php

namespace Acg\TesteBundle\Controller;

use Acg\TesteBundle\Entity\Produtos;
use Acg\TesteBundle\Form\ProdutosType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package Acg\TesteBundle\Controller
 *
 * @Route("/produtos")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="acg_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $produtos = $em->getRepository('AcgTesteBundle:Produtos')->findAll();

        if ($request->isMethod('POST')) {
            $titulo = $this->get('request')->request->get('titulo');
            $produtos = $em->getRepository('AcgTesteBundle:Produtos')->buscaPorTitulo($titulo);
        }

        return $this->render('AcgTesteBundle:Default:index.html.twig',
            array(
                'produtos' => $produtos
            )
        );
    }


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/show/{id}", name="acg_show")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $produto = $em->getRepository('AcgTesteBundle:Produtos')->findOneById($id);

        return $this->render('AcgTesteBundle:Default:show.html.twig',
            array(
                'produto' => $produto
            )
        );

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/new", name="acg_new")
     */
    public function newAction(Request $request)
    {

        $entity = new Produtos();
        $form = $this->createCreateForm($entity);

        return $this->render('AcgTesteBundle:Default:new.html.twig',
            array(
                'form' => $form->createView()
            )
        );

    }

    /**
     * @param Request $request
     *
     * @Route("/create", name ="acg_create")
     * @Method("POST")
     * @Template("AcgTesteBundle:Default:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Produtos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Produto Cadastrado com Sucesso!');

            return $this->redirect($this->generateUrl('acg_index'));
        }
        return array(
          'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * @param Produtos $entity
     * @return \Symfony\Component\Form\Form
     */
    public function createCreateForm(Produtos $entity)
    {
        $form = $this->createForm(new ProdutosType(), $entity, array(
            'action' => $this->generateUrl('acg_create') ,
            'method' => 'POST'
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Salvar',
            'attr' => array(
                'class' => 'btn btn-success'
            )
        ));

        return $form;

    }


    /**
     * @param Request $request
     * @param $id
     *
     * @Route("/{id}/edit", name="acg_edit")
     */
    public function editAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcgTesteBundle:Produtos')->findOneById($id);

        $editForm = $this->createEditForm($entity);

        return $this->render('AcgTesteBundle:Default:edit.html.twig', array(
            'edit_form' => $editForm->createView(),
        ));

    }

    /**
     * @param Request $request
     * @param $id
     *
     * @Route("/{id}", name="acg_update")
     * @Method("PUT")
     * @Template("AcgTesteBundle:Default:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcgTesteBundle:Produtos')->findOneById($id);

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if($editForm->isValid()){
            $em->flush();
            return $this->redirect($this->generateUrl('acg_index'));
        }

    }



    /**
     *
     * @param Produtos $entity
     * @return \Symfony\Component\Form\Form
     */
    public function createEditForm(Produtos $entity)
    {
        $form = $this->createForm(new ProdutosType(), $entity, array(
            'action' => $this->generateUrl('acg_update', array('id' => $entity->getId())) ,
            'method' => 'PUT'
        ));

        $form->add('submit', 'submit', array(
                'label' => 'Editar', 'attr' => array(
                    'class' => 'btn btn-success'
                )
            )
        );

        return $form;
    }
    /**
     * @param Request $request
     * @param $id
     *
     * @Route("/{id}/delete", name="acg_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcgTesteBundle:Produtos')->findOneById($id);

        if (!$entity){
            throw $this->createNotFoundException('Nãi foi possível trazer dados da Entidade!');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('acg_index'));

    }


}

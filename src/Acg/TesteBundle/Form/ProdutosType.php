<?php

namespace Acg\TesteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProdutosType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('descricao', 'textarea')
            ->add('categorias', 'entity', array(
                'class' => 'AcgTesteBundle:Categorias',
                'placeholder' => 'Selecione uma Categoria'
            ))
            ->add('ativo')
        ;
    }

    public function setDetaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
           'data_class' => 'Acg\TesteBundle\Entity\Produtos'
        ));
    }

    public function getName()
    {
        return 'acg_testebundle_produtos';
    }

}